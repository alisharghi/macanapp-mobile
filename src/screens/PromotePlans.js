import React , {useState , useEffect} from 'react'
import {View , Text , FlatList} from 'react-native'
import LinearGradient from 'react-native-linear-gradient'
import {styles} from "../styles";
import {getPlans} from './../api/promote'
import getRandom from './../styles/gradientRandom'
import {Button,Icon} from 'native-base'
import LoadingPage from './../components/loading'
import Header from './../components/Header'
import {getLogin} from './../redux/user'
import {setPlan} from './../redux/form'

function AdwordsPlans(props) {
    const [plans,setPlans] = useState()
    const [loading,setLoading] = useState(true)
    useEffect(()=>{
        getPlans().then(value=>{
            setPlans(value)
            console.log(value)
        }).catch(error=>{
            console.log(error)
        }).finally(()=>{
            setLoading(false)
        })
    },[])
    return(
        <View style={{flex:1}} >
        {
            loading?
            <LoadingPage message={'در حال دریافت اطلاعات'} />
            :
            <View style={styles.AdwordPlansView.background}>
            <Header title='پلن‌ها' />
            <FlatList
            style={{flex:1,width:'100%',paddingBottom:8}}
            data={plans}
            keyExtractor={(item,index)=>item.id}
            renderItem={({item})=>{
                return (
                    <View>
                    <LinearGradient start={{x:0,y:1}} end={{x:1,y:0}} style={styles.AdwordPlansView.gradientView} colors={getRandom()}>
                        <Text style={styles.AdwordPlansView.titleText}>
                            {item.name} 
                        </Text>
                        {item.planoptions.map((value,index)=>(
                            <Text key={index} style={styles.AdwordPlansView.descText}>
                                {value.desc} 
                            </Text>
                        ))}
                            <Text style={styles.AdwordPlansView.descText}>
                                {item.clickcount +" کلیک"} 
                            </Text>
                            <Text style={styles.AdwordPlansView.descText}>
                                {item.reachcount+ " ریچ"} 
                            </Text>
                            <Text style={styles.AdwordPlansView.descText}>
                                {item.viewcount + " ویو"} 
                            </Text>
                        <Button style={styles.AdwordPlansView.button} iconRight block bordered onPress={()=>{
                            if(getLogin()){
                                setPlan(item)
                                props.navigation.replace('PromoteForm')
                            }
                            else{
                                props.navigation.replace('Landing')
                            }
                        }}>
                        <Icon type='AntDesign' name='shoppingcart' style={{color:'#ffffff'}} />

                            <Text style={styles.AdwordPlansView.buyText}>
                                {item.price.toFixed(1).replace(/\d(?=(\d{3})+\.)/g, '$&,').slice(0,-2) + " تومان"}
                            </Text>
                        </Button>
                    </LinearGradient>
                    </View>
                )}}
            />
            </View>
        }
            
        </View>
    )
}

export default AdwordsPlans