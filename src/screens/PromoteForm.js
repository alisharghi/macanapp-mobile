import React from 'react'
import {styles} from './../styles/index'
import {Root,Text,Card,Textarea,Segment,Button, Icon,Spinner} from 'native-base'
import {useState} from 'react'
import { ScrollView } from 'react-native'
import {maincolor,statusbarcolor} from './../utils/index'
import {validateName,validateSite} from '../validation/adwords';
import { validateStep} from './../validation/promotes'
import {ProgressSteps,ProgressStep} from 'react-native-progress-steps'
import {getError,setReduxForm,getProgress, setError} from './../redux/form'
import {setAd} from './../api/promote'
import {connect} from 'trim-redux'
import MyInput from './../components/Input'
import ImagePicker from 'react-native-image-picker'
import Slider from '@ptomasroos/react-native-multi-slider'
import Header from './../components/Header'

function PromoteForm(props){
    const [loading,setLoading] = useState(false)
    let obj = {
        name:'',
        type:'پست',
        mediatype:'عکس',
        showtype:'reach',
        contry:'ایران',
        minage:13,
        maxage:65,
        gender:'هر دو',
        ostype:'هر دو',
        desc:'',
        media:'',
        mediaurl:''
    }
    const [formerr , setFormerr] = useState(obj)
    const [form , setForm] = useState(obj)
    const [selected , setSelected] = useState({type:1,mediatype:1,showtype:1,contry:1,gender:3,ostype:3})
    let load = loading?<Spinner color={maincolor}/>:null
    const [width,setWidth] = useState()
    const error = props.error
    const handlewidth = (e)=>{
        setWidth(e.nativeEvent.layout.width)
    }

    const setChange = (name,value,validate)=>{
        setForm(prev=>{
            return {...prev,...{[name]:value.value}}
        })
        setFormerr(prev=>{
            return {...prev,...{[name]:validate(value.value)}}
        })
    }

    const setSegment = (key,id,value)=>{
        setForm(prev=>{
            return {...prev,...{[key]:value}}
        })
        setSelected(prev=>{
            return {...prev,[key]:id}
        })
    }
    return(
        <Root>
        <Header title='فرم تبلیغات اینستاگرام' />
        <ScrollView keyboardShouldPersistTaps={'always'} style={styles.signUpView.scrollView.style} contentContainerStyle={styles.signUpView.scrollView.contentStyle}>
        <Card onLayout={handlewidth} style={styles.signUpView.card}>
        <ProgressSteps>
        {/* page1 */}
        <ProgressStep onNext={()=>validateStep(1,form,formerr)} errors={error}>
            
            <MyInput 
                name='name'
                icon='check'
                label='نام کمپین'
                error={formerr.name}
                validate={validateName}
                value={form.name}
                onChangeText={value=>{
                    setChange('name',value,validateName)
                }}
            />
            

            <Text style={styles.input.inputLabel}>نوع تبلیغ</Text>
            <Segment style={styles.AdwordFormView.segment.View}>
            <Button style={[selected.type===1?styles.AdwordFormView.segment.selectedButton:styles.AdwordFormView.segment.button,{borderTopLeftRadius:5,borderBottomLeftRadius:5}]} active={selected.type===1} onPress={()=>{
                setSegment('type',1,'پست')
            }}>
                <Text style={selected.type===1?styles.AdwordFormView.segment.selectedText:styles.AdwordFormView.segment.text}>
                    پست
                </Text>
            </Button>
            <Button style={[selected.type===2?styles.AdwordFormView.segment.selectedButton:styles.AdwordFormView.segment.button,{borderBottomRightRadius:5,borderTopRightRadius:5}]} active={selected.type===2} onPress={()=>{
                setSegment('type',2,'استوری')
            }}>
                <Text style={selected.type===2?styles.AdwordFormView.segment.selectedText:styles.AdwordFormView.segment.text}>
                    استوری
                </Text>
            </Button>
            </Segment>

            <Text style={styles.input.inputLabel}>نوع فایل</Text>
            <Segment style={styles.AdwordFormView.segment.View}>
            <Button style={[selected.mediatype===1?styles.AdwordFormView.segment.selectedButton:styles.AdwordFormView.segment.button,{borderTopLeftRadius:5,borderBottomLeftRadius:5}]} active={selected.mediatype===1} onPress={()=>{
                setSegment('mediatype',1,'عکس')
            }}>
                <Text style={selected.mediatype===1?styles.AdwordFormView.segment.selectedText:styles.AdwordFormView.segment.text}>
                    عکس
                </Text>
            </Button>
            <Button style={[selected.mediatype===2?styles.AdwordFormView.segment.selectedButton:styles.AdwordFormView.segment.button,{borderBottomRightRadius:5,borderTopRightRadius:5}]} active={selected.mediatype===2} onPress={()=>{
                setSegment('mediatype',2,'ویدیو')
            }}>
                <Text style={selected.mediatype===2?styles.AdwordFormView.segment.selectedText:styles.AdwordFormView.segment.text}>
                    ویدیو
                </Text>
            </Button>
            </Segment>

            <Text style={styles.input.inputLabel}>نوع نمایش</Text>
            <Segment style={styles.AdwordFormView.segment.View}>
            <Button style={[selected.showtype===1?styles.AdwordFormView.segment.selectedButton:styles.AdwordFormView.segment.button,{borderTopLeftRadius:5,borderBottomLeftRadius:5}]} active={selected.showtype===1} onPress={()=>{
                setSegment('showtype',1,'reach')
            }}>
                <Text style={selected.showtype===1?styles.AdwordFormView.segment.selectedText:styles.AdwordFormView.segment.text}>
                    reach
                </Text>
            </Button>
            <Button style={[selected.showtype===2?styles.AdwordFormView.segment.selectedButton:styles.AdwordFormView.segment.button,selected.mediatype==1?{borderBottomRightRadius:5,borderTopRightRadius:5}:{}]} active={selected.showtype===2} onPress={()=>{
                setSegment('showtype',2,'click')
            }}>
                <Text style={selected.showtype===2?styles.AdwordFormView.segment.selectedText:styles.AdwordFormView.segment.text}>
                    click
                </Text>
            </Button>
            {
                selected.mediatype===2?
                <Button style={[selected.showtype===3?styles.AdwordFormView.segment.selectedButton:styles.AdwordFormView.segment.button,{borderBottomRightRadius:5,borderTopRightRadius:5}]} active={selected.showtype===3} onPress={()=>{
                    setSegment('showtype',3,'view')
                }}>
                    <Text style={selected.showtype===3?styles.AdwordFormView.segment.selectedText:styles.AdwordFormView.segment.text}>
                        view
                    </Text>
                </Button>
                :null
            }
            </Segment>

            </ProgressStep>

            {/* page2 */}
            <ProgressStep onNext={()=>setError(true)}>
                <Text style={styles.input.inputLabel}>کشور نمایش</Text>
                <Segment style={styles.AdwordFormView.segment.View}>
                <Button style={[selected.contry===1?styles.AdwordFormView.segment.selectedButton:styles.AdwordFormView.segment.button,{borderTopLeftRadius:5,borderBottomLeftRadius:5}]} active={selected.contry===1} onPress={()=>{
                    setSegment('contry',1,'ایران')
                }}>
                    <Text style={selected.contry===1?styles.AdwordFormView.segment.selectedText:styles.AdwordFormView.segment.text}>
                        ایران
                    </Text>
                </Button>
                <Button style={[selected.contry===2?styles.AdwordFormView.segment.selectedButton:styles.AdwordFormView.segment.button,{borderBottomRightRadius:5,borderTopRightRadius:5}]} active={selected.contry===2} onPress={()=>{
                    setSegment('contry',2,'سایر')
                }}>
                    <Text style={selected.contry===2?styles.AdwordFormView.segment.selectedText:styles.AdwordFormView.segment.text}>
                        سایر کشورها
                    </Text>
                </Button>
                </Segment>

                <Text style={styles.input.inputLabel}>جنسیت کاربران</Text>
                <Segment style={styles.AdwordFormView.segment.View}>
                <Button style={[selected.gender===1?styles.AdwordFormView.segment.selectedButton:styles.AdwordFormView.segment.button,{borderTopLeftRadius:5,borderBottomLeftRadius:5}]} active={selected.gender===1} onPress={()=>{
                    setSegment('gender',1,'مرد')
                }}>
                    <Text style={selected.gender===1?styles.AdwordFormView.segment.selectedText:styles.AdwordFormView.segment.text}>
                        مرد
                    </Text>
                </Button>
                <Button style={selected.gender===2?styles.AdwordFormView.segment.selectedButton:styles.AdwordFormView.segment.button} active={selected.gender===2} onPress={()=>{
                    setSegment('gender',2,'زن')
                }}>
                    <Text style={selected.gender===2?styles.AdwordFormView.segment.selectedText:styles.AdwordFormView.segment.text}>
                        زن
                    </Text>
                </Button>
                <Button style={[selected.gender===3?styles.AdwordFormView.segment.selectedButton:styles.AdwordFormView.segment.button,{borderBottomRightRadius:5,borderTopRightRadius:5}]} active={selected.gender===3} onPress={()=>{
                    setSegment('gender',3,'هر دو')
                }}>
                    <Text style={selected.gender===3?styles.AdwordFormView.segment.selectedText:styles.AdwordFormView.segment.text}>
                        هر دو
                    </Text>
                </Button>
                </Segment>

                <Text style={styles.input.inputLabel}>سیستم عامل کاربران</Text>
                <Segment style={styles.AdwordFormView.segment.View}>
                <Button style={[selected.ostype===1?styles.AdwordFormView.segment.selectedButton:styles.AdwordFormView.segment.button,{borderTopLeftRadius:5,borderBottomLeftRadius:5}]} active={selected.ostype===1} onPress={()=>{
                    setSegment('ostype',1,'اندروید')
                }}>
                    <Text style={selected.ostype===1?styles.AdwordFormView.segment.selectedText:styles.AdwordFormView.segment.text}>
                        اندروید
                    </Text>
                </Button>
                <Button style={selected.ostype===2?styles.AdwordFormView.segment.selectedButton:styles.AdwordFormView.segment.button} active={selected.ostype===2} onPress={()=>{
                    setSegment('ostype',2,'ios')
                }}>
                    <Text style={selected.ostype===2?styles.AdwordFormView.segment.selectedText:styles.AdwordFormView.segment.text}>
                        ios
                    </Text>
                </Button>
                <Button style={[selected.ostype===3?styles.AdwordFormView.segment.selectedButton:styles.AdwordFormView.segment.button,{borderBottomRightRadius:5,borderTopRightRadius:5}]} active={selected.ostype===3} onPress={()=>{
                    setSegment('ostype',3,'هر دو')
                }}>
                    <Text style={selected.ostype===3?styles.AdwordFormView.segment.selectedText:styles.AdwordFormView.segment.text}>
                        هر دو
                    </Text>
                </Button>
                </Segment>

                <Text style={styles.input.inputLabel}>بازه سنی مخاطبان</Text>
                <Slider sliderLength={width-24} markerStyle={{backgroundColor:statusbarcolor}} containerStyle={{flex:1,width:'100%',paddingLeft:8,paddingRight:8}} selectedStyle={{backgroundColor:statusbarcolor}} onValuesChange={val=>{
                    setForm(prev=>{
                        return {...prev,minage:val[0],maxage:val[1]}
                    })
                }} values={[form.minage,form.maxage]} min={13} max={65} />
                <Text style={styles.input.inputLabel}>بین {form.minage} تا {form.maxage} سال</Text>
            </ProgressStep>


            <ProgressStep onNext={()=>validateStep(3,form,formerr)} errors={error}>
            <Text style={styles.input.inputLabel}>یکی از فیلدهای زیر را پرکنید</Text>

            <MyInput 
                name='mediaurl'
                icon='link'
                label='آدرس فایل در اینستا'
                error={formerr.mediaurl}
                validate={validateSite}
                value={form.mediaurl}
                onChangeText={value=>{
                    setChange('mediaurl',value,validateSite)
                }}
            />

            <Button onPress={()=>{
                ImagePicker.launchImageLibrary({mediaType:form.mediatype==='عکس'?'photo':'video'},response=>{
                    if(response.uri){
                        //fileSize
                        //width
                        //height
                        //uri
                        //type
                        //data
                        //path
                        setForm(prev=>{
                            return {...prev,media:response}
                        })
                    }
                    console.log(response)
                })
            }} style={{justifyContent:'center',marginLeft:8,marginRight:8}} block bordered>
            <Icon name='file-upload' type='MaterialIcons' style={{color:'#0055ff'}} />
            <Text style={styles.input.inputLabel}>انتخاب فایل</Text>
            </Button>
            {form.mediatype==='عکس'?
            <Text style={styles.input.inputLabel}>
                حجم فایل مجاز: حداکثر ۱۰ مگابایت{'\n'}
                فرمت های مجاز: gif, jpeg, jpg, png{'\n'}
                ابعاد کمتر از ۳۰۰ پیکسل نباشد{'\n'}
                ابعاد بیشتر از ۲۰۴۸ پیکسل نباشد{'\n'}
            </Text>:
            <Text style={styles.input.inputLabel}>
                حجم فایل مجاز: حداکثر ۱۰۰ مگابایت{'\n'}
                فرمت های مجاز: gif, mp4, mov{'\n'}
                وضوح (Resolution): حداقل 720p{'\n'}
                نسبت ابعاد پیشنهادی: (16:9){'\n'}
                زمان ویدیو : {form.type==='پست'?'حداکثر ۶۰ ثانیه\n':'۵ تا ۱۵ ثانیه\n'}
            </Text>
            }
            </ProgressStep>
            
            <ProgressStep  onSubmit={()=>{
                setReduxForm(form)
                setLoading(true)
                setAd().then(value=>{
                    props.navigation.navigate('Payment',{type:'promote'})
                })
                .catch(err=>{
                    console.log(err)
                })
                .finally(()=>{
                    setLoading(false)
                })
            }}>
            <Textarea value={form.tozihat} rowSpan={5} bordered placeholder={'توضیحات اضافی'} onChangeText={(e)=>{
                setForm(prev=>{
                        return {...prev,...{desc:e}}
                    })
            }} />
            </ProgressStep>
            </ProgressSteps>
            {load}
            {props.progress===0?null:<Text style={[styles.input.inputLabel,{textAlign:'center',fontSize:16,color:statusbarcolor}]}>{props.progress + '%'}</Text>}
            </Card>
            </ScrollView>
        </Root>
    )
}

const mstp = state =>({
    error : state.error,
    progress : state.progress
})
export default connect(mstp)(PromoteForm)