import React from 'react'
import {Text , TouchableOpacity} from 'react-native'
import LinearGradient from 'react-native-linear-gradient'
import {styles} from "../styles";
import {statusbarcolor} from './../utils/index'
import {Icon} from 'native-base'
import { ScrollView } from 'react-native-gesture-handler';
import {getLogin} from './../redux/user'

function Menu(props) {
    return(
        <LinearGradient style={styles.menuView.background} colors={[statusbarcolor,'#6DD5FA','#FFFFFF']}>
        <ScrollView keyboardShouldPersistTaps={'always'} style={{flex:1,alignSelf:'center'}} contentContainerStyle={{alignItems:'center',justifyContent:'center',paddingBottom:8}}>
        <TouchableOpacity activeOpacity={0.5} style={styles.menuView.tochable} onPress={()=>{
            props.navigation.navigate('AdwordsPlans')
        }}>
            <LinearGradient start={{x:0,y:1}} end={{x:1,y:0}} style={styles.menuView.gradientView} colors={['#8E2DE2','#B06AB3']}>
            <Icon name='google' type='AntDesign' style={{color:'#fff'}} />
            <Text style={styles.menuView.text}>
                تبلیغات گوگل
            </Text>
            </LinearGradient>
        </TouchableOpacity>
        <TouchableOpacity activeOpacity={0.5} style={styles.menuView.tochable} onPress={()=>{
            props.navigation.navigate('PromotePlans')
        }}>
            <LinearGradient start={{x:0,y:1}} end={{x:1,y:0}} style={styles.menuView.gradientView} colors={['#ee0979','#ff6a00']}>
            <Icon name='instagram' type='AntDesign' style={{color:'#fff'}} />
            <Text style={styles.menuView.text}>
                تبلیغات اینستاگرام
            </Text>
            </LinearGradient>
        </TouchableOpacity>

        <TouchableOpacity activeOpacity={0.5} style={styles.menuView.tochable} onPress={()=>{
            if(getLogin()){
                props.navigation.navigate('Orders')
            }
            else{
                props.navigation.replace('Landing')
            }
        }}>
            <LinearGradient start={{x:0,y:1}} end={{x:1,y:0}} style={styles.menuView.gradientView} colors={['#00b09b','#96c93d']}>
            <Icon name='profile' type='AntDesign' style={{color:'#fff'}} />
            <Text style={styles.menuView.text}>
                لیست سفارشات
            </Text>
            </LinearGradient>
        </TouchableOpacity>

        <TouchableOpacity activeOpacity={0.5} style={styles.menuView.tochable}>
            <LinearGradient start={{x:0,y:1}} end={{x:1,y:0}} style={styles.menuView.gradientView} colors={['#0052D4','#6FB1FC']}>
            <Icon name='rest' type='AntDesign' style={{color:'#fff'}} />
            <Text style={styles.menuView.text}>
                بزودی ...
            </Text>
            </LinearGradient>
        </TouchableOpacity>
        </ScrollView>
        </LinearGradient>
    )
}

export default Menu