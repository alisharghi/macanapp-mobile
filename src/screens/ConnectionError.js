import React , {useEffect} from 'react'
import Loading from './../components/loading'
import { BackHandler } from 'react-native'

function ConnectionError(props) {
    useEffect(()=>{
        BackHandler.addEventListener('hardwareBackPress',()=>{return true})
        return BackHandler.removeEventListener('hardwareBackPress',()=>{return false})
    })
    return(
        <Loading title="اینترنت قطع شد :(" desc="اینترنت خود را روشن کنید" />
    )
}

export default ConnectionError