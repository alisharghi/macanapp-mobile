import { statusbarcolor} from "../utils";
import React from 'react'
import {Text} from 'react-native'
import {styles} from "../styles";
import {Button , Icon} from 'native-base'
import LinearGradient from 'react-native-linear-gradient'

function Landing(props) {
    return(
        <LinearGradient colors={[statusbarcolor,'#6DD5FA','#FFFFFF']} style={styles.components.loading.View} >
        <Text numberOfLines={3} style={styles.LandingView.alertText}>
            برای استفاده از تمامی امکانات برنامه باید حساب کاربری داشته باشید
        </Text>
            <Button style={styles.LandingView.button} iconRight block success onPress={()=>{
                props.navigation.navigate('Login')
            }}>
                <Icon name='user' type='AntDesign' style={{color:'#fff'}} />
                <Text style={styles.LandingView.buttonText}>
                    ورود به حساب کاربری
                </Text>
            </Button>
            <Button style={styles.LandingView.button} iconRight block danger onPress={()=>{
                props.navigation.navigate('SignUp')
            }}>
                <Icon name='adduser' type='AntDesign' style={{color:'#fff'}} />
                <Text style={styles.LandingView.buttonText}>
                    ثبت نام
                </Text>
            </Button>
            <Button style={styles.LandingView.button} iconRight block warning onPress={()=>{
                props.navigation.navigate('Menu')
            }}>
                <Icon name='home' type='AntDesign' style={{color:'#fff'}} />
                <Text style={styles.LandingView.buttonText}>
                    مشاهده برنامه
                </Text>
            </Button>
        </LinearGradient>
    )
}

export default Landing