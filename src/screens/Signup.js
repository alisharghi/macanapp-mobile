import React from 'react'
import {Button,Text,Card,Toast,Root,Spinner,Icon} from 'native-base'
import {styles} from './../styles'
import {useState} from 'react'
import { signUp } from './../api/user'
import { ScrollView } from 'react-native'
import { validateUsername , validatePassword ,validateEmail} from './../validation/user'
import {maincolor} from './../utils/index'
import MyInput from './../components/Input'
import Header from './../components/Header'

function SignUp(props){
    const [User,setUser] = useState({username:'',email:'',password:''})
    const [loading,setLoading] = useState(false)
    const [formerr , setFormerr] = useState({})
    let load = loading?<Spinner color={maincolor}/>:null

    const handleClick = ()=>{
        if(User.username&&User.email&&User.password){
            if(!formerr.username&&!formerr.email&&!formerr.password){
                setLoading(true)
                signUp(User)
                .then(res=>{
                    if(res.status===200){
                        Toast.show({
                        text: 'ثبت نام انجام شد',
                        type:'success',
                        textStyle:styles.signUpView.toastText
                    })
                    props.navigation.navigate('Menu')
                    }
                    setLoading(false)
                    })
                .catch(err=>{
                    Toast.show({
                        text: err,
                        type:'danger',
                        textStyle:styles.signUpView.toastText
                    })
                    setLoading(false)
                })
            }
        }
        else{
            setFormerr(prev=>{
                return{...prev,...{username:validateUsername(User.username),email:validateEmail(User.email),password:validatePassword(User.password)}}
            })
        }
    }
    return(
        <Root>
        <Header title='ثبت نام' />
        <ScrollView keyboardShouldPersistTaps={'always'} style={styles.signUpView.scrollView.style} contentContainerStyle={styles.signUpView.scrollView.contentStyle}>
        <Card style={styles.signUpView.card}>
            <MyInput 
                name='name'
                icon='rightsquareo'
                label='نام'
                value={User.name}
                iconType='AntDesign'
                onChangeText={value=>{
                    setUser(prev=>{
                        return {...prev,...{name:value.value}}
                    })
                }}
            />

            <MyInput 
                name='family'
                icon='team'
                label='نام خانوادگی'
                value={User.family}
                iconType='AntDesign'
                onChangeText={value=>{
                    setUser(prev=>{
                        return {...prev,...{family:value.value}}
                    })
                }}
            />

            <MyInput 
                name='username'
                icon='user'
                label='نام کاربری'
                error={formerr.username}
                validate={validateUsername}
                value={User.username}
                iconType='AntDesign'
                onChangeText={value=>{
                    setUser(prev=>{
                        return {...prev,...{username:value.value}}
                    })
                    if(value.error!==formerr.username){
                        setFormerr(prev=>{
                            return{...prev,...{username:validateUsername(value.value)}}
                        })
                    }
                }}
            />

            <MyInput 
                name='email'
                icon='mail'
                label='ایمیل'
                error={formerr.email}
                validate={validateEmail}
                value={User.email}
                iconType='AntDesign'
                onChangeText={value=>{
                    setUser(prev=>{
                        return {...prev,...{email:value.value}}
                    })
                    if(value.error!==formerr.email){
                        setFormerr(prev=>{
                            return{...prev,...{email:validateEmail(value.value)}}
                        })
                    }
                }}
            />

            <MyInput 
                name='password'
                icon='Safety'
                label='رمز عبور'
                error={formerr.password}
                validate={validatePassword}
                value={User.password}
                secureTextEntry
                iconType='AntDesign'
                onChangeText={value=>{
                    setUser(prev=>{
                        return {...prev,...{password:value.value}}
                    })
                    if(value.error!==formerr.password){
                        setFormerr(prev=>{
                            return{...prev,...{password:validatePassword(value.value)}}
                        })
                    }
                }}
            />

            <MyInput 
                name='phone'
                icon='phone'
                label='تلفن تماس'
                value={User.phone}
                keyboardType='phone-pad'
                iconType='AntDesign'
                onChangeText={value=>{
                    setUser(prev=>{
                        return {...prev,...{phone:value.value}}
                    })
                }}
            />
            
            {load}
            <Button style={styles.signUpView.button} block success onPress={handleClick}>
            <Icon name='pluscircleo' active type='AntDesign' style={{color:'#fff'}} />
                <Text style={styles.LandingView.buttonText}>
                    ثبت نام
                </Text>
            </Button>
            </Card>
            </ScrollView>
        </Root>
    )
}

export default SignUp