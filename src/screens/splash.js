import React , {useEffect} from 'react'
import {me} from './../api/user'
import Loading from './../components/loading'
import connectionListener from './../redux/connection'

function Splash(props) {
    useEffect(()=>{
        connectionListener(props)
        me().then(res=>{
            props.navigation.replace('Menu')
        }).catch(err=>{
            if(err===401){
                props.navigation.replace('Landing')
            }
        })
    })
    return(
        <Loading title="شرکت تبلیغاتی ماکان" desc="تبلیغات در گوگل و اینستاگرام" />
    )
}

export default Splash