import {Text,Card,Root,Spinner,Segment,Textarea,Button,Icon} from 'native-base'
import {styles} from './../styles'
import React , {useState , useEffect} from 'react'
import { ScrollView ,View} from 'react-native'
import {maincolor} from './../utils/index'
import { validateStep, validateName, validateTitr, validateTozih, validateSite, validatePhone, validateZamime, validateTitleLink , validateKeywords } from '../validation/adwords';
import {ProgressSteps,ProgressStep} from 'react-native-progress-steps'
import {setReduxForm} from './../redux/form'
import {setAd} from './../api/adwords'
import {connect} from 'trim-redux'
import MyInput from './../components/Input'
import Header from './../components/Header'
import AdwordExample from './../components/AdwordExample'
import Modal from 'react-native-modal'

function AdwordsForm(props){
    const [loading,setLoading] = useState(false)
    let obj = {
        name:'',
        titr1:'',
        titr2:'',
        tozih1:'',
        tozih2:'',
        site:'',
        keywords:'',
        strategy:'بالای صفحه',
        zamime1:'',
        zamime2:'',
        zamime3:'',
        zamime4:'',
        link1:'',
        link2:'',
        link3:'',
        link4:'',
        titlelink1:'',
        titlelink2:'',
        titlelink3:'',
        titlelink4:'',
        phone:'',
        tozihat:''
    }
    const [formerr , setFormerr] = useState(obj)
    const [form , setForm] = useState(obj)
    const [selected , setSelected] = useState(3)
    const [display,setDisplay] = useState(false)
    const [page,setPage] = useState(1)
    
    // useEffect(()=>{
    //     console.log(form)
    // },[form])
    let load = loading?<Spinner color={maincolor}/>:null
    let error = props.error
    const setChange = (name,value,validate)=>{
        setForm(prev=>{
            return {...prev,...{[name]:value.value}}
        })
        if(value.error!==formerr[name]){
            setFormerr(prev=>{
                return {...prev,...{[name]:validate(value.value)}}
            })
        }
    }

    const setSegment = (id,value)=>{
        setForm(prev=>{
            return {...prev,...{strategy:value}}
        })
        setSelected(id)
    }

    const Fragment1 = ()=>{
        return (
            <View>
                <MyInput 
                name='name'
                icon='check'
                label='نام کمپین'
                error={formerr.name}
                validate={validateName}
                value={form.name}
                onChangeText={value=>{
                    setChange('name',value,validateName)
                }}
            />

            <MyInput 
                name='titr1'
                icon='filter-1'
                label='تیتر اول'
                error={formerr.titr1}
                validate={validateTitr}
                value={form.titr1}
                onChangeText={value => setChange('titr1',value,validateTitr)}
            />
            
            <MyInput 
                name='titr2'
                icon='filter-2'
                label='تیتر دوم'
                error={formerr.titr2}
                validate={validateTitr}
                value={form.titr2}
                onChangeText={value => setChange('titr2',value,validateTitr)}
            />

            <MyInput 
                name='tozih1'
                icon='filter-1'
                label='توضیح اول'
                error={formerr.tozih1}
                validate={validateTozih}
                value={form.tozih1}
                onChangeText={value => setChange('tozih1',value,validateTozih)}
            />

            <MyInput 
                name='tozih2'
                icon='filter-2'
                label='توضیح دوم'
                error={formerr.tozih2}
                validate={validateTozih}
                value={form.tozih2}
                onChangeText={value => setChange('tozih2',value,validateTozih)}
            />

            <MyInput 
                name='site'
                icon='web'
                label='سایت'
                error={formerr.site}
                validate={validateSite}
                value={form.site}
                onChangeText={value => setChange('site',value,validateSite)}
            />
            
            <Segment style={styles.AdwordFormView.segment.View}>
            <Button style={[selected===1?styles.AdwordFormView.segment.selectedButton:styles.AdwordFormView.segment.button,{borderTopLeftRadius:5,borderBottomLeftRadius:5}]} info active={selected===1} onPress={()=>{
                setSegment(1,'پایین صفحه')
            }}>
                <Text style={selected===1?styles.AdwordFormView.segment.selectedText:styles.AdwordFormView.segment.text}>
                    پایین صفحه
                </Text>
            </Button>
            <Button style={selected===2?styles.AdwordFormView.segment.selectedButton:styles.AdwordFormView.segment.button} info active={selected===2} onPress={()=>{
                setSegment(2,'لینک اول')
            }}>
                <Text style={selected===2?styles.AdwordFormView.segment.selectedText:styles.AdwordFormView.segment.text}>
                    لینک اول
                </Text>
            </Button>
            <Button style={[selected===3?styles.AdwordFormView.segment.selectedButton:styles.AdwordFormView.segment.button,{borderTopRightRadius:5,borderBottomRightRadius:5}]} info active={selected===3} onPress={()=>{
                setSegment(3,'بالای صفحه')
            }}>
                <Text style={selected===3?styles.AdwordFormView.segment.selectedText:styles.AdwordFormView.segment.text}>
                    بالای صفحه
                </Text>
            </Button>
            </Segment>
            </View>
        )
    }

    const Fragment2= ()=>{
        return(
            <View>
                <MyInput 
                name='phone'
                icon='phone'
                label='شماره'
                error={formerr.phone}
                validate={validatePhone}
                value={form.phone}
                onChangeText={value => setChange('phone',value,validatePhone)}
            />

            <MyInput 
                name='zamime1'
                icon='filter-1'
                label='ضمیمه اول'
                error={formerr.zamime1}
                validate={validateZamime}
                value={form.zamime1}
                onChangeText={value => setChange('zamime1',value,validateZamime)}
            />

            <MyInput 
                name='zamime2'
                icon='filter-2'
                label='ضمیمه دوم'
                error={formerr.zamime2}
                validate={validateZamime}
                value={form.zamime2}
                onChangeText={value => setChange('zamime2',value,validateZamime)}
            />

            <MyInput 
                name='zamime3'
                icon='filter-3'
                label='ضمیمه سوم'
                error={formerr.zamime3}
                validate={validateZamime}
                value={form.zamime3}
                onChangeText={value => setChange('zamime3',value,validateZamime)}
            />

            <MyInput 
                name='zamime4'
                icon='filter-4'
                label='ضمیمه چهارم'
                error={formerr.zamime4}
                validate={validateZamime}
                value={form.zamime4}
                onChangeText={value => setChange('zamime4',value,validateZamime)}
            />

            <MyInput 
                name='keywords'
                icon='label'
                label='کلمات کلیدی'
                error={formerr.keywords}
                validate={validateKeywords}
                value={form.keywords}
                onChangeText={value => setChange('keywords',value,validateKeywords)}
            />
            </View>
        )
    }

    const Fragment3 = ()=>{
        return(
            <View>
                <MyInput 
                name='link1'
                icon='filter-1'
                label='لینک اول'
                error={formerr.link1}
                validate={validateSite}
                value={form.link1}
                onChangeText={value => setChange('link1',value,validateSite)}
            />

            <MyInput 
                name='titlelink1'
                icon='filter-1'
                label='عنوان لینک اول'
                error={formerr.titlelink1}
                validate={validateTitleLink}
                value={form.titlelink1}
                onChangeText={value => setChange('titlelink1',value,validateTitleLink)}
            />
            
            <MyInput 
                name='link2'
                icon='filter-2'
                label='لینک دوم'
                error={formerr.link2}
                validate={validateSite}
                value={form.link2}
                onChangeText={value => setChange('link2',value,validateSite)}
            />

            <MyInput 
                name='titlelink2'
                icon='filter-2'
                label='عنوان لینک دوم'
                error={formerr.titlelink2}
                validate={validateTitleLink}
                value={form.titlelink2}
                onChangeText={value => setChange('titlelink2',value,validateTitleLink)}
            />

            <MyInput 
                name='link3'
                icon='filter-3'
                label='لینک سوم'
                error={formerr.link3}
                validate={validateSite}
                value={form.link3}
                onChangeText={value => setChange('link3',value,validateSite)}
            />

            <MyInput 
                name='titlelink3'
                icon='filter-3'
                label='عنوان لینک سوم'
                error={formerr.titlelink3}
                validate={validateTitleLink}
                value={form.titlelink3}
                onChangeText={value => setChange('titlelink3',value,validateTitleLink)}
            />

            <MyInput 
                name='link4'
                icon='filter-4'
                label='لینک چهارم'
                error={formerr.link4}
                validate={validateSite}
                value={form.link4}
                onChangeText={value => setChange('link4',value,validateSite)}
            />

            <MyInput 
                name='titlelink4'
                icon='filter-4'
                label='عنوان لینک چهارم'
                error={formerr.titlelink4}
                validate={validateTitleLink}
                value={form.titlelink4}
                onChangeText={value => setChange('titlelink4',value,validateTitleLink)}
            />
            </View>
        )
    }

    const Fragment4 = ()=>{
        return(
            <View>
                <Textarea value={form.tozihat} rowSpan={5} bordered placeholder={'توضیحات اضافی'} onChangeText={(e)=>{
                    setForm(prev=>{
                            return {...prev,...{tozihat:e}}
                        })
                }} />
            </View>
        )
    }
    
    return(
        <Root>
        <Header title='فرم تبلیغات گوگل' />
        <Modal
            style={{justifyContent:'flex-end',margin:0}}
            onBackdropPress={()=>setDisplay(false)}
            onBackButtonPress={()=>setDisplay(false)}
            isVisible={display} useNativeDriver={true} >
        <View style={{flexDirection:'column',width:'100%',justifyContent:'space-evenly',backgroundColor:'#fff',borderRadius:8,padding:8}}>
            <AdwordExample {...form} />
            <Button style={{borderRadius:8}} onPress={()=>setDisplay(false)} block danger>
            <Text style={styles.signUpView.toastText}>
            بستن
            </Text>
            </Button>
        </View>
        </Modal>
        <ScrollView keyboardShouldPersistTaps={'always'} style={styles.signUpView.scrollView.style} contentContainerStyle={styles.signUpView.scrollView.contentStyle}>
        <Card style={styles.signUpView.card}>
        <ProgressSteps>
        {/* page1 */}
            <ProgressStep 
                onNext={()=>{
                    validateStep(1,form,formerr).then(()=>{
                        setPage(2)
                    }).catch(()=>{})
                }}
                errors={error}>
            
            {page==1?Fragment1():null}

            </ProgressStep>
            {/* page2 */}
            <ProgressStep 
                onNext={()=>{
                    validateStep(2,form,formerr).then(()=>{
                        setPage(3)
                    }).catch(()=>{})
                }} 
                onPrevious={()=>{
                    validateStep(1,form,formerr).then(()=>{
                    setPage(1)
                    }).catch(()=>{})}}
                errors={error}>

            {page==2?Fragment2():null}
            
            </ProgressStep>
            {/* page3 */}
            <ProgressStep 
                onNext={()=>{
                    validateStep(3,form,formerr).then(()=>{
                        setPage(4)
                    }).catch(()=>{})
                }} 
                onPrevious={()=>{
                    validateStep(2,form,formerr).then(()=>{
                        setPage(2)
                    }).catch(()=>{})
                }}
                errors={error}>
            
            {page==3?Fragment3():null}
            
            </ProgressStep>
            <ProgressStep
                onPrevious={()=>{
                    validateStep(3,form,formerr).then(()=>{
                        setPage(3)
                    }).catch(()=>{})
                }}
                onSubmit={()=>{
                    setReduxForm(form)
                    setLoading(true)
                    setAd().then(value=>{
                        console.log(value)
                        console.log(value.id)
                        console.log(value.googleplans.price)
                        props.navigation.navigate('Payment',{type:'adword'})
                    })
                    .catch(err=>{

                    })
                    .finally(()=>{
                        setLoading(false)
                    })
                }}>
            
            {page==4?Fragment4():null}
            </ProgressStep>
            </ProgressSteps>
            <Button style={[{justifyContent:'space-between'},styles.stepView.button]} iconRight block warning onPress={()=>setDisplay(true)}>
                <Icon name='phone-android' active type='MaterialIcons' style={{color:'#fff',marginLeft:16}} />
                <Text style={styles.LandingView.buttonText}>
                    پیش‌نمایش آگهی
                </Text>
            </Button>
            {load}
            </Card>
            </ScrollView>
        </Root>
    )
}

const mstp = state =>({
    error : state.error
})

export default connect(mstp)(AdwordsForm)