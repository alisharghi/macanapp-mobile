import React from 'react'
import {Button,Text,Card,Toast,Root,Spinner,Icon} from 'native-base'
import {styles} from './../styles'
import {useState} from 'react'
import { login } from './../api/user'
import { validateUsername , validatePassword } from './../validation/user'
import {maincolor} from './../utils/index'
import MyInput from './../components/Input'
import Header from './../components/Header'
import {ScrollView} from 'react-native'

function Login(props){
    const [User,setUser] = useState({identifier:'',password:''})
    const [formerr,setFormerr] = useState({identifier:'',password:''})
    const [loading,setLoading] = useState(false)
    let load = loading?<Spinner color={maincolor}/>:null
    const handleClick = ()=>{
        if(User.identifier.length>0&&User.password.length>0){
            if(!formerr.identifier&&!formerr.password){
                setLoading(true)
                login(User)
                .then(res=>{
                    if(res.status===200){
                        props.navigation.navigate('Menu')
                    }
                    setLoading(false)
                    })
                .catch(err=>{
                    Toast.show({
                        text: err,
                        type:'danger',
                        textStyle:styles.LoginView.toastText
                    })
                    setLoading(false)
                })
            }
        }
    }
    return(
        <Root>
        <Header title='ورود به حساب کاربری' />
        <ScrollView keyboardShouldPersistTaps={'always'} style={styles.signUpView.scrollView.style} contentContainerStyle={styles.signUpView.scrollView.contentStyle}>
        <Card style={styles.signUpView.card}>
            <MyInput 
                name='identifier'
                icon='user'
                label='نام کاربری'
                iconType='AntDesign'
                error={formerr.identifier}
                validate={validateUsername}
                value={User.identifier}
                onChangeText={value=>{
                    setUser(prev=>{
                        return {...prev,...{identifier:value.value}}
                    })
                    if(value.error!==formerr.identifier){
                        setFormerr(prev=>{
                            return {...prev,...{identifier:value.error}}
                        })
                    }
                }}
            />
            <MyInput 
                name='password'
                icon='Safety'
                label='رمز عبور'
                error={formerr.password}
                validate={validatePassword}
                value={User.password}
                secureTextEntry
                iconType='AntDesign'
                onChangeText={value=>{
                    setUser(prev=>{
                        return {...prev,...{password:value.value}}
                    })
                    if(value.error!==formerr.password){
                        setFormerr(prev=>{
                            return {...prev,...{password:value.error}}
                        })
                    }
                }}
            />
            {load}
            <Button style={styles.signUpView.button} iconRight block success onPress={handleClick}>
                <Icon name='login' active type='AntDesign' style={{color:'#fff'}} />
                <Text style={styles.LandingView.buttonText}>
                    ورود به حساب کاربری
                </Text>
            </Button>
            </Card>
            </ScrollView>
        </Root>
    )
}

export default Login