import React,{useEffect,useState} from 'react'
import { View , Text , Linking } from 'react-native'
import {styles} from './../styles/index'
import { Spinner } from 'native-base'
import {Icon} from 'native-base'
import {init, verify} from './../api/payment'

export default function Payment(props){
    const [done,setDone] = useState([])
    const [doing,setDoing] = useState('در حال اتصال به بانک')
    const [first,setFirst] = useState(true)
    let params = props.navigation.state.params
    useEffect(()=>{
        init(params).then((data)=>{
            setDone(prev=>{
                return [...prev,'در حال اتصال به بانک']
            })
            setDoing('در حال انتقال به درگاه پرداخت')
            Linking.openURL(data.payinit.url)
        })
        Linking.addEventListener('url', (value)=>{
            setDone(prev=>{
                return [...prev,'در حال انتقال به درگاه پرداخت']
            })
            setDoing('در حال اعتبار سنجی')
            verify().then((value)=>{
                setDone(prev=>{
                    return [...prev,'در حال اعتبار سنجی']
                })
                if(value.refID){
                    setDone(prev=>{
                        return [...prev,'کد پیگیری: ' + value.refID]
                    })
                    setDoing('در حال برگشت')
                    setTimeout(()=>{
                        props.navigation.replace('Orders')
                    },5000)
                }
                else{
                    setDoing('خطا در انجام تراکنش')
                    setTimeout(()=>{
                        props.navigation.replace('Orders')
                    },5000)
                }
            })
        });
    },[first])
    return(
        <View style={styles.components.loading.View} >
        {
            done.map((value,index)=>{
                return(
                    <View key={index} style={styles.payment.view}>
                        <Icon type='MaterialIcons' name='check' style={{color:'#fff'}} />
                        <Text style={styles.payment.text}>
                            {value + "  "}
                        </Text>
                    </View>
                )
            })
        }
            <View style={styles.payment.view}>
                <Spinner size={'small'} color='#ffffff' />
                <Text style={styles.payment.text}>
                    {doing + "  "}
                </Text>
            </View>
        </View>
    )
}