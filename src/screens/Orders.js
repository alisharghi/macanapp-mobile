import React ,{useEffect,useState} from 'react'
import {Root,Spinner} from 'native-base'
import {styles} from './../styles'
import {maincolor, font} from './../utils/index'
import Header from './../components/Header'
import {FlatList,Text,ScrollView} from 'react-native'
import {getPayments,getAdwords,getPromotes} from './../api/Orders'
import RenderOrders from './../components/renderOrders'
import RenderPayments from '../components/RenderPayments';
import {getUserID,getToken} from './../redux/user'

function Orders(props){
    const [loading,setLoading] = useState({payments:true,adwords:true,promotes:true})
    let load = <Spinner color={maincolor}/>
    const [payments,setPayments] = useState([])
    const [adwords,setAdwords] = useState([])
    const [promotes,setPromotes] = useState([])

    useEffect(()=>{
        let token = getToken()
        let uid = getUserID()
        getPayments(token,uid).then(res=>{
            setPayments(res)
            setLoading({payments:false})
        })
        getAdwords(token,uid).then(res=>{
            setAdwords(res)
            setLoading({adwords:false})
        })
        getPromotes(token,uid).then(res=>{
            setPromotes(res)
            setLoading({promotes:false})
        })
    },[])

    return(
        <Root>
        <Header title='لیست سفارشات' />
        <ScrollView keyboardShouldPersistTaps={'always'} style={styles.signUpView.scrollView.style} contentContainerStyle={styles.signUpView.scrollView.contentStyle}>
        <Text style={{textAlign:'right',fontFamily:font,fontSize:14,color:'#666',padding:6,borderRadius:4}}>پرداخت‌ها</Text>
        {loading.payments?load:
        <FlatList 
            data={payments}
            contentContainerStyle={{paddingBottom:4}}
            keyExtractor={(item,index)=>index.toString()}
            renderItem={({item})=><RenderPayments item={item} />}
            ListEmptyComponent={<Text style={{textAlign:'center',fontFamily:font,fontSize:12,color:'#666',padding:4}}>شما تاکنون پرداختی انجام نداده‌اید</Text>}
        />
        }
        <Text style={{textAlign:'right',fontFamily:font,fontSize:14,color:'#666',padding:6,borderRadius:4}}>سفارشات گوگل</Text>
        {loading.adwords?load:
        <FlatList 
            data={adwords}
            contentContainerStyle={{paddingBottom:4}}
            keyExtractor={(item,index)=>index.toString()}
            renderItem={({item})=><RenderOrders item={item} type='adword' />}
            ListEmptyComponent={<Text style={{textAlign:'center',fontFamily:font,fontSize:12,color:'#666',padding:4}}>شما تاکنون سفارشی ثبت نکرده‌اید</Text>}
        />
        }
        <Text style={{textAlign:'right',fontFamily:font,fontSize:14,color:'#666',padding:6,borderRadius:4}}>سفارشات اینستاگرام</Text>
        {loading.promotes?load:
        <FlatList 
            data={promotes}
            contentContainerStyle={{paddingBottom:4,marginBottom:8}}
            keyExtractor={(item,index)=>index.toString()}
            renderItem={({item})=><RenderOrders item={item} type='promote' />}
            ListEmptyComponent={<Text style={{textAlign:'center',fontFamily:font,fontSize:12,color:'#666',padding:4}}>شما تاکنون سفارشی ثبت نکرده‌اید</Text>}
        />
        }
        </ScrollView>
        </Root>
    )
}

export default Orders