const colors = [
    ['#f74a4a','#fc6767'],
    ['#6a3093','#8643b5'],
    ['#8E2DE2','#a949fc'],
    ['#e63094','#b91d73'],
    ['#f12711','#fa412d'],
    ['#334ccc','#3F5EFB'],
    ['#00b09b','#018f7e'],
    ['#4568DC','#3d5bbf'],
    ['#7b4397','#a459c9'],
    ['#fe8c00','#fa9d2a'],
    ['#FF4E50','#e34648'],
    ['#4089ed','#3a7bd5'],
    ['#33b8b3','#299490'],
    ['#FF512F','#ff6e52'],
    ['#4364F7','#6FB1FC'],
    ['#8E2DE2','#B06AB3']
]

function getRand(){
    return(colors[Math.floor(Math.random()*colors.length)])
}

export default getRand