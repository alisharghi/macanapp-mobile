import { maincolor , font, statusbarcolor } from "../utils";
import { StyleSheet} from 'react-native'
const style =StyleSheet.create({
    container : {
        shadowColor : '#ec008c',
    }
})
const styles = {
    input:{
        view:{
            felx:1,
            justifyContent:'center',
            alignItems:'center'
        },
        inputLabel:{
            fontFamily: font,
            color : '#777777',
            fontSize:13,
            marginRight:8,
            marginTop:4
        },
        inputFloatingLabel:{
            fontFamily: font,
            color : '#777777',
            fontSize:13,
        },
        inputStyle:{
            color:'#444444',
            marginLeft:8,
            marginRight:8,
            marginTop:8,
        },
        inputError:{
            color : '#EF5350',
            fontSize : 13,
            fontFamily: font
        }
    },
    components:{
        loading:{
            View:{
                backgroundColor: maincolor,
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center'
            },
            titleText:{
                color : '#ffffff',
                fontSize : 16,
                paddingBottom : 8,
                fontFamily: font
            },
            descText:{
                color : '#ffffff',
                fontSize : 14,
                paddingBottom : 20,
                fontFamily: font
            }
        },
        Header:{
            parent:{
                width:'100%',
                backgroundColor:'#eee'
            },
            header:{
                height:50,
                backgroundColor:statusbarcolor,
                justifyContent:'center',
                borderBottomRightRadius:18,
                borderBottomLeftRadius:18,
                elevation:5
            },
            title:{
                fontFamily:font,
                color:'#fff',
                textAlign:'center',
                fontSize:17
            }
        }
    },
    payment:{
        view:{
            flexDirection:'row',
            alignItems:'center',
            paddingBottom:8
        },
        text:{
            fontFamily:font,
            color:'#fff',
            fontSize:14
        }
    },
    menuView:{
        background : {
            backgroundColor: '#DBDBDB',
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            flexDirection:'row'
        },
        gradientView:{
            padding : 28,
            borderRadius:8,
            flexDirection:'row',
            justifyContent:'space-between'

        },
        tochable:{
            width:'70%',
            margin:8,
            elevation:8,
            borderRadius:16
        },
        text: {
            color : '#ffffff',
            fontSize : 16,
            fontFamily: font,
            alignSelf:'center'
        }
    },
    AdwordPlansView:{
        background : {
            backgroundColor: '#eee',
            flex: 1,
            justifyContent: 'flex-start',
            alignItems: 'center',
        },
        gradientView:{
            width:'95%',
            margin:4,
            padding : 16,
            borderRadius:8,
            elevation:5,
            alignSelf:'center'
        },
        titleText: {
            color : '#ffffff',
            fontSize : 16,
            fontFamily: font,
            alignSelf:'center'
        },
        descText:{
            color : '#eee',
            fontSize : 12,
            padding:4,
            fontFamily: font,
            alignSelf:'center'
        },
        buyText: {
            color : '#ffffff',
            fontSize : 14,
            fontFamily: font,
            alignSelf:'center'
        },
        button:{
            padding:4,
            borderRadius:8,
            borderColor:'#fff'
        }
    },
    LandingView:{
        buttonText:{
            fontFamily: font,
            color : '#ffffff'
        },
        button:{
            paddingLeft : 8,
            paddingRight : 8,
            marginLeft:80,
            marginRight:80,
            marginBottom : 4,
            borderRadius:8
        },
        alertText:{
            fontFamily: font,
            color : '#fff',
            paddingLeft : 8,
            paddingRight : 8,
            marginLeft:80,
            marginRight:80,
            marginBottom : 4,
            textAlign:'center'
        }
    },
    AdwordFormView:{
        segment:{
            View:{
                backgroundColor:'transparent',
                flex:1,
                marginLeft:8,
                marginRight:8,
                marginBottom:4,
                height:'auto',
                minHeight:42,
                borderColor:statusbarcolor,
                borderWidth:1,
                borderRadius:8
            },
            selectedButton:{
                flex:1,
                justifyContent:'center',
                height:'100%',
                backgroundColor:statusbarcolor,
                borderColor:statusbarcolor
            },
            button:{
                flex:1,
                justifyContent:'center',
                height:'100%',
                backgroundColor:'#fff',
            },
            selectedText:{
                fontFamily:font,
                fontSize:13,
                color:'#fff'
            },
            text:{
                fontFamily:font,
                fontSize:13,
                color:statusbarcolor
            }
        },
    },
    signUpView:{
        background : {
            backgroundColor: maincolor,
            flex:1,
            justifyContent:'center',
            paddingTop:8,
            paddingBottom:8
        },
        button:{
            paddingLeft : 40,
            paddingRight : 40,
            marginLeft:8,
            marginRight:8,
            marginBottom : 4,
            borderRadius:8
        },
        card:{
            justifyContent: 'center',
            alignItems: 'center',
            borderRadius:8,
            marginLeft:30,
            marginRight:30,
            marginBottom:16
        },
        text: {
            color : '#fff',
            fontSize : 16,
            fontFamily: font,
            textAlign:'center'
        },
        toastText: {
            color : '#ffffff',
            fontSize : 13,
            fontFamily: font
        },
        scrollView:{
            style:{
                width:'100%',
                height:'100%',
                alignSelf:'center',
                paddingTop:8,
                paddingBottom:8,
                backgroundColor:'#eee',
            },
            contentStyle:{
                alignItems:'center',
            }
        }
    },
    stepView:{
        button:{
            paddingLeft : 8,
            paddingRight : 8,
            marginLeft:8,
            marginRight:8,
            marginBottom : 4,
            borderRadius:8
        },
    },
    LoginView:{
        background : {
            backgroundColor: maincolor,
            flex: 1,
            justifyContent: 'center'
        },
        card:{
            justifyContent: 'center',
            alignItems: 'center',
            borderRadius:8,
            marginLeft:30,
            marginRight:30
        },
        text: {
            color : '#fff',
            fontSize : 16,
            paddingBottom : 8,
            fontFamily: font,
            textAlign:'center'
        },
        toastText: {
            color : '#ffffff',
            fontSize : 13,
            fontFamily: font
        }
    }

}
export {styles}