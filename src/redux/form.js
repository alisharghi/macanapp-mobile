import {setStore,getStore} from 'trim-redux'

export function setPlan(plan){
    //for buy
    // console.log('redux setPlan',plan)
    setStore('plan',plan)
}

export function getPlan(){
    return getStore('plan')
}

export function setProgress(progress){
    setStore('progress',progress)
}

export function getProgress(){
    return getStore('progress')
}

export function getPlanID(){
    return getStore('plan').id
}

export function setReduxForm(value){
    // console.log('redux setFormValue',value)
    setStore('form',value)
    getForm()
}

export function getForm(){
    // console.log('redux getForm',getStore('form'))
    return getStore('form')
}

export function setError(value){
    if(getError()!==value){
        console.log('redux setError',value)
        setStore('error',value)
    }
}

export function getError(){
    // console.log('redux getError',getStore('error'))
    return getStore('error')
}

export function setSubmittedID(value){
    setStore('submitedID',value)
}

export function getSubmittedID(){
    // console.log('redux getError',getStore('error'))
    return getStore('submitedID')
}