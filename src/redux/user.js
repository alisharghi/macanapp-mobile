import {setStore,getStore} from 'trim-redux'

export function setToken(token){
    // console.log('redux setToken : ',token)
    setStore('token',token)
}

export function getToken(){
    // console.log('redux getToken : ',getStore('token'))
    return getStore('token')
}

export function setUser(user){
    // console.log('redux setUser : ',user)
    setStore('user',user)
}

export function getUser(){
    // console.log('redux getUser : ',getStore('user'))
    return getStore('user')
}

export function getUserID(){
    // console.log('redux getUser : ',getStore('user'))
    return getStore('user')._id
}

export function setLogin(value){
    setStore('isLogedIn',value)
}

export function getLogin(){
    return getStore('isLogedIn')
}