import NetInfo from '@react-native-community/netinfo'
import {setStore,getStore} from 'trim-redux'

export default function checkConnectionEvent(props){
    NetInfo.addEventListener(state=>{
        if(state.type==='none'){
            setConnection('none')
            props.navigation.navigate('ConnectionError')
        }
        else{
            if(getConnection()==='none'){
                setConnection(state.type)
                props.navigation.pop()
            }
        }
    })
}

function setConnection(type){
    setStore('connection',type)
}

function getConnection(type){
    return getStore('connection','none')
}