import {setStore,getStore} from 'trim-redux'

export function setPayID(value){
    setStore('payID',value)
}

export function getPayID(){
    return getStore('payID')
}