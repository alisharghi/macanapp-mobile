import { createStore } from 'trim-redux'

const state = {
    token : '',
    user : {},
    plan:{},
    form:{},
    error:true,
    connection:'',
    submitedID:null,
    payID:null,
    isLogedIn:false,
    progress:0
}

export const store = createStore(state)