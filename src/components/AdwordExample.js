import React from 'react'
import {View,Text} from 'react-native'
import { font } from '../utils';
import {Icon} from 'native-base'

export default function AdwordExample(props){
    return(
        <View style={{backgroundColor:'#f5f5f5',padding:8,minHeight:20}}>
        <View style={{flexDirection:'row-reverse'}}>
        <Text style={{color:'#1b67bd',fontFamily:font,fontSize:14}}>{props.titr1 || 'تیتر اول'} | </Text>
        <Text style={{color:'#1b67bd',fontFamily:font,fontSize:14}}>{props.titr2 || 'تیتر دوم'}</Text>
        </View>
        <View style={{flexDirection:'row',justifyContent:'flex-end'}}>
            <Text style={{color:'#006621',fontSize:12,borderColor:'#006621',borderWidth:1,borderRadius:1,padding:2}}>Ad</Text>
            <Text style={{color:'#006621',fontSize:12,paddingLeft:4}}>{props.site || 'www.google.com'}</Text>
        </View>
        <View style={{borderBottomColor:'#ebebeb',borderBottomWidth:1,marginBottom:4,marginTop:4}} />
        <View style={{flexDirection:'row-reverse'}}>
            <Text style={{fontFamily:font,fontSize:11,color:'#545454'}}>{props.tozih1 || 'توضیح اول'} . </Text>
            <Text style={{fontFamily:font,fontSize:11,color:'#545454'}}>{props.tozih2 || 'توضیح دوم'}</Text>
        </View>
        <View style={{flexDirection:'row-reverse'}}>
            <Text style={{fontFamily:font,fontSize:11,color:'#545454'}}>{props.zamime1 || 'ضمیمه اول'} . </Text>
            <Text style={{fontFamily:font,fontSize:11,color:'#545454'}}>{props.zamime2||'ضمیمه دوم'} . </Text>
            <Text style={{fontFamily:font,fontSize:11,color:'#545454'}}>{props.zamime3||'ضمیمه سوم'} . </Text>
            <Text style={{fontFamily:font,fontSize:11,color:'#545454'}}>{props.zamime4||'ضمیمه چهارم'}</Text>
        </View>
        <View>
            <Text style={{fontFamily:font,fontSize:13,color:'#1b67bd',padding:2}}>{props.titlelink1||'عنوان لینک اول'}</Text>
            <Text style={{fontFamily:font,fontSize:13,color:'#1b67bd',padding:2}}>{props.titlelink2||'عنوان لینک دوم'}</Text>
            <Text style={{fontFamily:font,fontSize:13,color:'#1b67bd',padding:2}}>{props.titlelink3||'عنوان لینک سوم'}</Text>
            <Text style={{fontFamily:font,fontSize:13,color:'#1b67bd',padding:2}}>{props.titlelink4||'عنوان لینک چهارم'}</Text>
        </View>
        <View style={{borderBottomColor:'#ebebeb',borderBottomWidth:1,marginBottom:4,marginTop:4}} />
        <View style={{flexDirection:'row-reverse'}}>
             <Icon name='phone' type='AntDesign' style={{color:'#666',fontSize:22}} />
             <Text style={{marginRight:10,color:'#666',fontFamily:font,fontSize:13}}>
             {props.phone||'شماره تماس'}
             </Text>
        </View>
        </View>
    )
}