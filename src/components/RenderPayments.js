import {View,Text,TouchableOpacity} from 'react-native'
import {styles} from './../styles/index'
import {font,maincolor} from './../utils/index'
import React,{useState,useEffect} from 'react'
import Modal from 'react-native-modal'
import {Button,Icon} from 'native-base'

export default function RenderPayments(props){
    const [show,setShow] = useState(false)
    useEffect(()=>{
        return ()=>setShow(false)
    },[])
    return(
        <TouchableOpacity onPress={()=>setShow(!show)} activeOpacity={0.5} style={{flex:1,margin:4,marginLeft:8,marginRight:8,padding:8,borderRadius:4,backgroundColor:'#fff',elevation:4,flexDirection:'row-reverse',width:'95%',alignSelf:'center',alignItems:'center',justifyContent:'space-evenly'}}>
        <View style={{flex:7,flexDirection:'column',alignItems:'flex-end'}}>
            <Text style={styles.input.inputLabel}>{'شناسه پرداخت : '+props.item.bankid}</Text>
            <Text style={styles.input.inputLabel}>
            {'مبلغ فاکتور : ' + props.item.price + ' تومان'}
            </Text>
            <Modal
                style={{justifyContent:'flex-end',margin:0}}
                onBackdropPress={()=>setShow(false)}
                onBackButtonPress={()=>setShow(false)}
                isVisible={show} useNativeDriver={true} >
            <View style={{flexDirection:'column',width:'100%',justifyContent:'space-evenly',backgroundColor:'#fff',borderRadius:8,padding:8}}>
                <Text style={styles.input.inputLabel}>{'شناسه پرداخت : '+props.item.bankid}</Text>
                <Text style={styles.input.inputLabel}>
                {'مبلغ فاکتور : ' + props.item.price + ' تومان'}
                </Text>
                {props.item.refID?
                <Text style={{...styles.input.inputLabel,fontSize:14,color:'#77f'}}>
                پرداخت شده
                </Text>:
                <Text style={{...styles.input.inputLabel,fontSize:14,color:'#ff7766'}}>
                لغو شده
                </Text>
                }
                <Text style={styles.input.inputLabel}>
                {Math.round((new Date().getTime()-new Date(props.item.createdAt).getTime())/86400000) + ' روز پیش'}
                </Text>
                <Button style={{borderRadius:8}} onPress={()=>setShow(false)} block danger>
                <Text style={styles.signUpView.toastText}>
                بستن
                </Text>
                </Button>
            </View>
            </Modal>
            </View>
            <Icon name={'payment'} active type={'MaterialIcons'} style={[{fontSize:28,flex:1},props.item.refID?{color:'#77f'}:{color:'#ff7766'}]} />
            <Icon name={'info-outline'} active type={'MaterialIcons'} style={{flex:1,fontSize:28,color:maincolor}} />
        </TouchableOpacity>
    )
}