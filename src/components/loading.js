import React,{useEffect} from 'react'
import { View , Text , StatusBar} from 'react-native'
import {styles} from './../styles/index'
import { Spinner } from 'native-base'
import {statusbarcolor} from './../utils/index'

export default function loading(props){
    useEffect(()=>{
        StatusBar.setBarStyle("light-content")
        StatusBar.setBackgroundColor(statusbarcolor)
    },[])
    return(
        <View style={styles.components.loading.View} >
            {
                props.title?
                <Text style={styles.components.loading.titleText}>
                    {props.title}
                </Text>
            :null
            }
            {
                props.desc?
                <Text style={styles.components.loading.descText}>
                    {props.desc}
                </Text>
                :null
            }
            <Spinner color='#ffffff' />
            {
                props.message?
                <Text style={styles.components.loading.descText}>
                    {props.message}
                </Text>
                :null
            }
        </View>
    )
}