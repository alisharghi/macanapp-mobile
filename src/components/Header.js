import {View,Text} from 'react-native'
import {styles} from './../styles/index'
import React from 'react'

export default function Header(props){
    return(
        <View style={styles.components.Header.parent}>
        <View style={styles.components.Header.header}>
            <Text style={styles.components.Header.title}>{props.title}</Text>
        </View>
        </View>
    )
}