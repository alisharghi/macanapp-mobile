import {View,Text,TouchableOpacity} from 'react-native'
import {styles} from './../styles/index'
import {font,maincolor} from './../utils/index'
import React , {useState} from 'react'
import Modal from 'react-native-modal'
import {Button,Icon} from 'native-base'
import {withNavigation} from 'react-navigation'
import {setSubmittedID,setPlan} from './../redux/form'
import AdwordExample from './AdwordExample'

function RenderOrders(props){
    const [show,setShow] = useState(false)
    const [showReport,setShowReport] = useState(false)
    const [showPreview,setShowPreview] = useState(false)
    function RenderReportPromote(){
        return(
            <View style={{flexDirection:'column',width:'100%',justifyContent:'space-evenly',backgroundColor:'#fff',borderRadius:8,padding:8}}>
                <Text style={styles.input.inputLabel}>{'تعداد نمایش یکتا : '+props.item.reportpromote.reach}</Text>
                <Text style={styles.input.inputLabel}>{'تعداد کل نمایش : '+props.item.reportpromote.impressions}</Text>
                {props.item.reportpromote['video-view']?<Text style={styles.input.inputLabel}>{'تعداد بازدید ویدیو : '+props.item.reportpromote['video-view']}</Text>:null}
                {props.item.reportpromote['link-click']?<Text style={styles.input.inputLabel}>{'تعداد کلیک بر روی لینک : '+props.item.reportpromote['link-click']}</Text>:null}
                {props.item.reportpromote.like?<Text style={styles.input.inputLabel}>{'تعداد لایک : '+props.item.reportpromote.like}</Text>:null}
                {props.item.reportpromote.comment?<Text style={styles.input.inputLabel}>{'تعداد کامنت : '+props.item.reportpromote.comment}</Text>:null}
                {props.item.reportpromote['saved-count']?<Text style={styles.input.inputLabel}>{'تعداد ذخیره : '+props.item.reportpromote['saved-count']}</Text>:null}
                {props.item.reportpromote.forward?<Text style={styles.input.inputLabel}>{'تعداد فوروارد : '+props.item.reportpromote.forward}</Text>:null}
                <Button style={{borderRadius:8}} onPress={()=>setShowReport(false)} block danger>
                <Text style={styles.signUpView.toastText}>
                بستن
                </Text>
                </Button>
            </View>
        )
    }
    function RenderReportAdword(){
        return(
            <View style={{flexDirection:'column',width:'100%',justifyContent:'space-evenly',backgroundColor:'#fff',borderRadius:8,padding:8}}>
                <Text style={styles.input.inputLabel}>{'تعداد کلیک : '+props.item.reportadword.clicks}</Text>
                <Text style={styles.input.inputLabel}>{'تعداد نمایش : '+props.item.reportadword.impreissions}</Text>
                <Text style={styles.input.inputLabel}>{'نسبت نمایش به کلیک : '+props.item.reportadword.ctr}</Text>
                <Text style={styles.input.inputLabel}>{'هزینه هر کلیک : '+props.item.reportadword['avg-cpc']}</Text>
                <Text style={styles.input.inputLabel}>{'هزینه کل : '+props.item.reportadword.cost}</Text>
                <Button style={{borderRadius:8}} onPress={()=>setShowReport(false)} block danger>
                <Text style={styles.signUpView.toastText}>
                بستن
                </Text>
                </Button>
            </View>
        )
    }
    function RenderDetails(){
        return(
            <View style={{flexDirection:'column',width:'100%',justifyContent:'space-evenly',backgroundColor:'#fff',borderRadius:8,padding:8}}>
                <Text style={styles.input.inputLabel}>{'نام کمپین : '+props.item.name}</Text>
                <Text style={styles.input.inputLabel}>
                    وضعیت آگهی : {props.item.status!=='پرداخت نشده'?
                                    <Text style={{fontFamily:font,fontSize:12,color:'#77f'}}>
                                    {props.item.status}
                                    </Text>:
                                    <Text style={{fontFamily:font,fontSize:12,color:'#ff7766'}}>
                                    {props.item.status}
                                    </Text>
                                    }
                </Text>
                
                <Text style={styles.input.inputLabel}>
                {'زمان ایجاد آگهی : ' + Math.round((new Date().getTime()-new Date(props.item.createdAt).getTime())/86400000) + ' روز پیش'}
                </Text>
                <Text style={styles.input.inputLabel}>
                {'زمان تغییر وضعیت آگهی : ' + Math.round((new Date().getTime()-new Date(props.item.updatedAt).getTime())/86400000) + ' روز پیش'}
                </Text>
                {props.item.titr1?
                    <Button style={{borderRadius:8}} onPress={()=>{
                        setShowPreview(true)
                    }} block warning>
                    <Text style={styles.signUpView.toastText}>
                    پیش‌نمایش آگهی
                    </Text>
                    <Modal
                        style={{justifyContent:'flex-end',margin:0}}
                        onBackdropPress={()=>setShowPreview(false)}
                        onBackButtonPress={()=>setShowPreview(false)}
                        isVisible={showPreview} useNativeDriver={true} >
                        <View style={{flexDirection:'column',width:'100%',justifyContent:'space-evenly',backgroundColor:'#fff',borderRadius:8,padding:8}}>
                            <AdwordExample {...props.item} />
                            <Button style={{borderRadius:8}} onPress={()=>setShowPreview(false)} block danger>
                                <Text style={styles.signUpView.toastText}>
                                بستن
                                </Text>
                            </Button>
                        </View>
                    </Modal>
                    </Button>:null
                }
                {props.item.status==='پرداخت نشده'?
                <Button style={{borderRadius:8,marginTop:4,marginBottom:4}} onPress={()=>{
                    setSubmittedID(props.item.id)
                    setPlan(props.item.payment)
                    console.log(props.item)
                    setShow(false)
                    props.navigation.navigate('Payment',{type:props.type})
                }} block success>
                <Text style={styles.signUpView.toastText}>
                پرداخت
                </Text>
                </Button>:null
                }
                {props.item.reportadword?
                <Button style={{borderRadius:8}} onPress={()=>setShowReport(true)} block info>
                <Text style={styles.signUpView.toastText}>
                گزارش
                </Text>
                <Modal
                    style={{justifyContent:'flex-end',margin:0}}
                    onBackdropPress={()=>setShowReport(false)}
                    onBackButtonPress={()=>setShowReport(false)}
                    isVisible={showReport} useNativeDriver={true} >
                <RenderReportAdword />
                </Modal>
                </Button>:null
                }
                {props.item.reportpromote?
                <Button style={{borderRadius:8}} onPress={()=>setShowReport(true)} block info>
                <Text style={styles.signUpView.toastText}>
                گزارش
                </Text>
                <Modal
                    style={{justifyContent:'flex-end',margin:0}}
                    onBackdropPress={()=>setShowReport(false)}
                    onBackButtonPress={()=>setShowReport(false)}
                    isVisible={showReport} useNativeDriver={true} >
                <RenderReportPromote />
                </Modal>
                </Button>:null
                }
                <Button style={{borderRadius:8}} onPress={()=>setShow(false)} block danger>
                <Text style={styles.signUpView.toastText}>
                بستن
                </Text>
                </Button>
            </View>
        )
    }
    return(
        <TouchableOpacity onPress={()=>setShow(true)} activeOpacity={0.5} style={{flex:1,margin:4,marginLeft:8,marginRight:8,padding:8,borderRadius:4,backgroundColor:'#fff',elevation:4,flexDirection:'row-reverse',width:'95%',alignSelf:'center',alignItems:'center',justifyContent:'space-evenly'}}>
            <View style={{flex:7,flexDirection:'column',alignItems:'flex-end'}}>
                <Text style={styles.input.inputLabel}>{'نام کمپین : '+props.item.name}</Text>
                <Text style={styles.input.inputLabel}>
                {Math.round((new Date().getTime()-new Date(props.item.createdAt).getTime())/86400000) + ' روز پیش'}
                </Text>
            </View>
            {
                props.item.status!=='پرداخت نشده'?
                <Icon name={'check-circle'} active type={'MaterialIcons'} style={{fontSize:28,flex:1,color:'#77f'}} />:
                <Icon name={'cancel'} active type={'MaterialIcons'} style={{fontSize:28,flex:1,color:'#ff7766'}} />
            }
            <Icon name={'info-outline'} active type={'MaterialIcons'} style={{flex:1,fontSize:28,color:maincolor}} />
            <Modal
                style={{justifyContent:'flex-end',margin:0}}
                onBackdropPress={()=>setShow(false)}
                onBackButtonPress={()=>setShow(false)}
                isVisible={show} useNativeDriver={true} >
            <RenderDetails />
            </Modal>
        </TouchableOpacity>
    )
}

export default withNavigation(RenderOrders)