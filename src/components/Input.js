import React , {useState , useEffect } from 'react'
import { View , Text} from 'react-native'
import { Input , Label , Icon , Item} from 'native-base'
import {styles} from './../styles/index'

export default function MyInput(props){
    const [value,setValue] = useState(props.value)
    const [error,setError] = useState(props.error||null)

    
    const inputProps = {keyboardType:props.keyboardType,secureTextEntry:props.secureTextEntry}
    const handleChange = (val)=>{
        setValue(val)
        try{
            setError(props.validate(val))
            props.onChangeText({error:props.validate(val),value:val})
        }
        catch{
            props.onChangeText({value:val})
        }
        
    }
    return(
        <View style={styles.input.view}>
        <Item error={error?true:false} floatingLabel style={styles.input.inputStyle}>
            <Icon name={props.icon} type={props.iconType || 'MaterialIcons'} style={{color:'#777'}} />
            <Label style={styles.input.inputFloatingLabel}>
            {props.label}
            </Label>
            <Input {...inputProps} value={value} style={styles.input.inputStyle} 
                onChangeText={handleChange}
            />
        </Item>
        {error?<Text style={styles.input.inputError}>{error}</Text>:null}
        </View>
    )
}