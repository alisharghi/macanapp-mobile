import {url} from './../utils/index'
import Axios from 'axios'

export async function getPayments(token,uid){
    console.log('get Paymens')
    return new Promise((resolve,reject)=>{
        Axios.get(url+'payments?user='+uid+'&_sort=updatedAt:desc',{
            headers:{
                Authorization: 'Bearer ' + token
            }
        }).then(res=>{
            console.log('receive Paymens')
            resolve(res.data)
        }).catch(e=>{
            reject(e)
        })
    })
}

export async function getAdwords(token,uid){
    console.log('get Adwords')
    return new Promise((resolve,reject)=>{
        Axios.get(url+'adwords?user='+uid+'&_sort=updatedAt:desc',{
            headers:{
                Authorization: 'Bearer ' + token
            }
        }).then(res=>{
            console.log('receive Adwords')
            resolve(res.data)
        }).catch(e=>{
            reject(e)
        })
    })
}

export async function getPromotes(token,uid){
    console.log('get Promotes')
    return new Promise((resolve,reject)=>{
        Axios.get(url+'promotes?user='+uid+'&_sort=updatedAt:desc',{
            headers:{
                Authorization: 'Bearer ' + token
            }
        }).then(res=>{
            console.log('receive Promotes')
            resolve(res.data)
        }).catch(e=>{
            reject(e)
        })
    })
}