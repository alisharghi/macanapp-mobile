import { url } from '../utils/index'
import Axios from 'axios';
import {getToken,getUser,getUserID} from '../redux/user'
import {getPlan,getSubmittedID} from '../redux/form'
import {setPayID,getPayID} from './../redux/payments'

export async function init(props) {
    let data = await {price:getPlan().price,email:getUser().email,mobile:getUser().phone,platform:'android',...props,adID:getSubmittedID(),user:getUserID()}
    console.log(data)
    return new Promise((resolve,reject)=>{
        Axios.post(url+'pay/init',data,{
            headers: {
                Authorization: 'Bearer ' + getToken()
            }
        }).then(response => {
            setPayID(response.data.paymentsadd.id)
            console.log(response.data)
            resolve(response.data)
        }).catch(e=>{
            console.log(e.response.status)
            reject(e.response.status)
        })
    })
}

export async function verify() {
    return new Promise((resolve,reject)=>{
        Axios.get(url + 'payments/' + getPayID(),{
            headers: {
                Authorization: 'Bearer ' + getToken()
            }
        }).then(response => {
            console.log(response.data)
            resolve(response.data)
        }).catch(e=>{
            console.log(e.response.status)
            reject(e.response.status)
        })
    })
}