import {url} from './../utils/index'
import Axios from 'axios'
import {getForm,getPlan,getSubmittedID,setSubmittedID} from './../redux/form'
import {getUserID,getToken} from './../redux/user'
import {setProgress} from './../redux/form'

async function getPlans(){
    return new Promise((resolve,reject)=>{
        Axios.get(url+'promoteplans?active=true&_sort=id:desc').then(value=>{
            if(value.status===200){
                console.log(value)
                resolve(value.data)
            }
        }).catch(error=>{
            reject(error.response)
        })
    })
}

async function setAd(){
    console.log('send request')
    let sendData = await {...getForm(),user:getUserID(),promoteplans:getPlan().id,media:''}
    let id = await getSubmittedID()
    if(id===null){
        return new Promise((resolve,reject)=>{
            Axios.post(url+'promotes',sendData,{
                headers: {
                    Authorization: 'Bearer ' + getToken()
                },
            }).then(value=>{
                console.log('Submited',value)
                if(value.status===200){
                    setSubmittedID(value.data.id)
                    if(getForm().media){
                        upload().then((res)=>{
                            resolve({...value.data,media:res.url})
                        }).catch(e=>{
                            reject(e)
                        })
                    }
                    //uploading
                    else resolve(value.data)
                }
            }).catch(error=>{
                console.log('error',error)
                reject(error.response)
            })
        })
    }
    else{
        return new Promise((resolve,reject)=>{
            Axios.put(url+'promotes/'+id,sendData,{
                headers: {
                    Authorization: 'Bearer ' + getToken()
                }
            }).then(value=>{
                console.log('Updated',value)
                if(value.status===200){
                    if(getForm().media){
                        upload().then((res)=>{
                            resolve({...value.data,media:res._id})
                        }).catch(e=>{
                            reject(e)
                        })
                    }
                    //uploading
                    else resolve(value.data)
                }
            }).catch(error=>{
                console.log('error',error)
                reject(error.response)
            })
        })
    }
}

async function upload(){
    console.log('uploading')
    let sendData = await getForm().media
    // let sendData = sendData = { uri : 'content://media/external/images/media/395872' }
    let id = await getSubmittedID()
    let data = new FormData();
    data.append('files',{
        uri:sendData.uri,
        name:'test.jpg',
        type: 'multipart/form-data'
    })
    data.append('refId',id)
    data.append('ref','promote')
    data.append('field','media')
    return new Promise((resolve,reject)=>{
        Axios.post(url+'upload',data,{
            headers: {
                Authorization: 'Bearer ' + getToken()
            },
            // headers: {
            //     Authorization: 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1ZDdiODg1YzVhYTQ2ZTVhYzA1MzRiYTIiLCJpYXQiOjE1Njg2NDgzNDUsImV4cCI6MTU3MTI0MDM0NX0.rqldHUTR5_WyfoFpcQ1gv8HDd6T18cTFbaRdAKz9XMs'
            // },
            'Content-Type': 'multipart/form-data',
            onUploadProgress:p=>{
                setProgress((p.loaded/p.total).toFixed(0)*100)
            }
        }).then(value=>{
            console.log('Uploaded',value.data[0])
            setProgress(0)
            if(value.status===200){
                // setSubmittedID(value.data.id)
                //uploading
                resolve(value.data[0])
            }
        }).catch(error=>{
            console.log('error',error)
            reject(error.response)
        })
    })
}


export { getPlans , setAd , upload }