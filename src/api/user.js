import { url } from './../utils/index'
import AsyncStorage from '@react-native-community/async-storage'
import Axios from 'axios';
import {getToken,setToken,setUser,setLogin} from './../redux/user'

async function me() {
    if(!getToken()){
        await AsyncStorage.getItem('token').then(token=>{
            setToken(token)
        })
    }
    return new Promise((resolve,reject)=>{
        Axios.get(url+'users/me',{
            headers: {
                Authorization: 'Bearer ' + getToken()
            }
        }).then(response => {
            setLogin(true)
            setUser(response.data)
            resolve(response.data)
        }).catch(e=>{
            console.log(e)
            reject(e.response.status)
        })
    })
}

async function signUp(user){
    return new Promise((resolve,reject)=>{
        Axios.post(url+'users',user)
        .then(res=>{
            login({identifier:user.username,password:user.password}).then(val=>{
                resolve(val)
            }).catch(error=>{
                console.log(error)
            })
        })
        .catch(err=>{
            console.log('err',err)
            if(err.response.status===400){
                reject('نام کاربری یا ایمیل قبلا ثبت شده')
            }
        })
    })
}

async function login(user){
    return new Promise((resolve,reject)=>{
        Axios.post(url+'auth/local',user)
        .then(res=>{
            AsyncStorage.multiSet([['token',res.data.jwt],['user',JSON.stringify(res.data.user)]]).then(val=>{
                // console.log(val)
            }).catch(err=>{
                // console.log(err)
            })
            setLogin(true)
            setToken(res.data.jwt)
            setUser(res.data.user)
            resolve(res)
        })
        .catch(err=>{
            if(err.response.status===400){
                reject('نام کاربری یا رمز عبور اشتباه است.')
            }
        })
    })
}

export {me , login , signUp}