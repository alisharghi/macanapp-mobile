import {url} from './../utils/index'
import Axios from 'axios'
import { getToken,getUser, getUserID } from './../redux/user'
import { getForm , getPlan ,getSubmittedID,setSubmittedID} from './../redux/form'

export async function getPlans(){
    return new Promise((resolve,reject)=>{
        Axios.get(url+'googleplans?active=true&_sort=id:desc').then(value=>{
            if(value.status===200){
                resolve(value.data)
            }
        }).catch(error=>{
            reject(error.response)
        })
    })
}

export async function getEnum(){
    return new Promise((resolve,reject)=>{
        Axios.get(url+'adwords/enum',{
            headers: {
                Authorization: 'Bearer ' + getToken()
            }
        }).then(value=>{
            if(value.status===200){
                resolve(value.data)
            }
        }).catch(error=>{
            reject(error.response)
        })
    })
}

export async function setAd(){
    let sendData = await {...getForm(),user:getUserID(),googleplans:getPlan().id}
    let id = await getSubmittedID()
    if(id===null){
        return new Promise((resolve,reject)=>{
            Axios.post(url+'adwords',sendData,{
                headers: {
                    Authorization: 'Bearer ' + getToken()
                }
            }).then(value=>{
                console.log('Submited',value)
                if(value.status===200){
                    setSubmittedID(value.data.id)
                    resolve(value.data)
                }
            }).catch(error=>{
                console.log('error',error)
                reject(error.response)
            })
        })
    }
    else{
        return new Promise((resolve,reject)=>{
            Axios.put(url+'adwords/'+id,sendData,{
                headers: {
                    Authorization: 'Bearer ' + getToken()
                }
            }).then(value=>{
                console.log('Updated',value)
                if(value.status===200){
                    resolve(value.data)
                }
            }).catch(error=>{
                console.log('error',error)
                reject(error.response)
            })
        })
    }
}