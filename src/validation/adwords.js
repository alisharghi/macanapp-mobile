import {setError} from './../redux/form'

export function validateStep(step,form,err) {
    return new Promise((resolve,reject)=>{
        switch(step){
            case 1:
                if(!form.name||!form.titr1||!form.titr2||!form.tozih1||!form.tozih2||!form.site){
                    setError(true)
                    reject()
                }
                else{
                    if(err.name||err.titr1||err.titr2||err.tozih1||err.tozih2||err.site){
                        setError(true)
                        reject()
                    }
                    else{
                        setError(false)
                        resolve()
                    }
                }
                break
    
            case 2:
                if(!form.keywords||!form.zamime1||!form.zamime2||!form.zamime3||!form.zamime4||!form.phone){
                    setError(true)
                    reject()
                }
                else{
                    if(err.keywords||err.zamime1||err.zamime2||err.zamime3||err.zamime4||err.phone){
                        setError(true)
                        reject()
                    }
                    else{
                        setError(false)
                        resolve()
                    }
                }
                break
            case 3:
                if(!form.link1||!form.link2||!form.link3||!form.link4||!form.titlelink1||!form.titlelink2||err.titlelink3||err.titlelink4){
                    setError(true)
                    reject()
                }
                else{
                    if(err.link1||err.link2||err.link3||err.link4||err.titlelink1||err.titlelink2||err.titlelink3||err.titlelink4){
                        setError(true)
                        reject()
                    }
                    else{
                        setError(false)
                        resolve()
                    }
                }
                break
        }
    })
}


export function validateName(value) {
    let error = '';
    if (value.length<3) {
      error = 'حداقل باید ۳ کاراکتر باشد'
    }
    else if(value.length>35){
        error = 'حداکثر باید ۳۵ کاراکتر باشد'
    }
    return error;
}

export function validateTitr(value) {
    let error = '';
    if (value.length<2) {
      error = 'حداقل باید ۲ کاراکتر باشد'
    }
    else {
        if (value.length>30) {
            error = 'حداکثر باید ۳۰ کاراکتر باشد'
        }
    }
    return error;
}

export function validateTozih(value) {
    let error = '';
    if (value.length<3) {
      error = 'حداقل باید ۳ کاراکتر باشد'
    }
    else {
        if (value.length>90) {
            error = 'حداکثر باید ۹۰ کاراکتر باشد'
        }
    }
    return error;
}

export function validateSite(value) {
    let error = '';
    if (value.length<6) {
      error = 'حداقل باید ۶ کاراکتر باشد'
    }
    return error;
}


export function validatePhone(value) {
    let error = '';
    if (value.length<6) {
        error = 'حداقل باید ۶ کاراکتر باشد'
    }
    else {
        if(!value.match(/^[0-9.]+$/)){
            error = 'شماره تلفن معتبر وارد کنید'
        }
        else if (value.length>11) {
            error = 'حداکثر باید ۱۱ کاراکتر باشد'
        }
    }
    
    return error;
}

export function validateZamime(value) {
    let error = '';
    if (value.length>25) {
      error = 'حداکثر باید ۲۵ کاراکتر باشد'
    }
    return error;
}

export function validateTitleLink(value) {
    let error = '';
    if (value.length>25) {
      error = 'حداکثر باید ۲۵ کاراکتر باشد'
    }
    return error;
}


export function validateKeywords(value) {
    let error = '';
    if (value.length<1) {
      error = 'حداقل یک کلمه کلیدی وارد کنید'
    }
    return error;
}