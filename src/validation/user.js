function validateUsername(value) {
    let error;
    if (value.length<3) {
      error = 'حداقل باید ۳ کاراکتر باشد'
    }
    return error;
}

function validatePassword(value) {
    let error;
    if (value.length<6) {
      error = 'حداقل باید ۶ کاراکتر باشد'
    }
    return error;
}

function validateEmail(value) {
    let error;
    if (value.length<6) {
      error = 'حداقل باید ۶ کاراکتر باشد'
    }
    else{
        if(!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)){
            error = 'ایمیل معتبر وارد کنید';
        }
    }
    return error;
}

export { validateUsername , validatePassword , validateEmail } 