import React from 'react';
import Splash from './src/screens/splash'
import Landing from './src/screens/landing'
import Login from './src/screens/Login'
import Signup from './src/screens/Signup'
import Menu from './src/screens/menu'
import AdwordsPlans from './src/screens/AdwordsPlans'
import PromotePlans from './src/screens/PromotePlans'
import AdwordsForm from './src/screens/AdwordsForm'
import ConnectionError from './src/screens/ConnectionError'
import Payment from './src/screens/Payment'
import PromoteForm from './src/screens/PromoteForm'
import Orders from './src/screens/Orders'
import { createAppContainer} from "react-navigation";
import { createStackNavigator } from 'react-navigation-stack'
import { Provider } from 'trim-redux'
import { store } from './src/redux/store'
import {I18nManager} from 'react-native'

const AppNavigator = createStackNavigator({
  Home: {
    screen: Splash,
  },
  Landing: {
    screen: Landing,
  },
  Login:{
    screen : Login
  },
  SignUp:{
    screen: Signup
  },
  Menu:{
    screen: Menu
  },
  AdwordsPlans:{
    screen : AdwordsPlans
  },
  PromotePlans:{
    screen : PromotePlans
  },
  AdwordsForm:{
    screen:AdwordsForm
  },
  ConnectionError:{
    screen:ConnectionError
  },
  Payment:{
    screen:Payment,
  },
  PromoteForm:{
    screen:PromoteForm
  },
  Orders:{
    screen:Orders
  }
},
    {
      initialRouteName : 'Home',
      defaultNavigationOptions:{
        header:null,
      }
    });

const Navigation = createAppContainer(AppNavigator)

function storeProvider(props){
  I18nManager.forceRTL(false)
  I18nManager.allowRTL(false)
  return(
    <Provider store={store}>
      <Navigation />
    </Provider>
  )
}
export default storeProvider;
