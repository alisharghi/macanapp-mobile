import React from 'react';
import { View } from 'react-native';

const ProgressButtons = props => (
  <View style={{ flexDirection: 'column'}}>
    <View>{props.renderPreviousButton()}</View>
    <View>{props.renderNextButton()}</View>
  </View>
);

export default ProgressButtons;
